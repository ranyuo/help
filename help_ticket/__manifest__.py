{
    'name': 'Help Tickets',
    'description': 'Help Tickets',
    'summary': 'Help ticket de BigOdoo',
    'author': 'BigOdoo',
    'website': 'https://bigodoo.com',
    'depends': [
        'base',
        'website_helpdesk_support_ticket'
    ],
    'data': [
        'views/assets.xml',
        'views/ticket_view.xml'
    ],
    'qweb': ["static/src/xml/help_ticket.xml"],
    'installable': True,
    'application': True,
}
