import re
from odoo import models, fields, api, _
from odoo.http import request
import logging
from odoo.addons.bo_backend_sale_invoice_ticket.models.number_to_letter import to_word
logger = logging.getLogger(__name__)


class HelpdeskSupport(models.Model):
    _inherit = 'helpdesk.support'

    @api.model
    def check_user_group(self):
        uid = request.session.uid
        return True

    def btn_ticket(self):
        return {
            'name': 'Imprimir Ticket',
            'tag': 'help_ticket',
            'type': 'ir.actions.client',
            'params': {
                'ticket_id': self.id,
                'model_id': 'helpdesk.support'
            }
        }

    @api.model
    def helpdesk_data(self, ticket_id):
        helpdesk_env = self.browse(ticket_id)
        company_env = self.env['res.company'].browse(helpdesk_env.company_id.id)
        fields_company = {'currency_id', 'email', 'website', 'company_registry', 'vat', 'name', 'phone',
                          'partner_id', 'country_id', 'state_id', 'city',
                          'tax_calculation_rounding_method', 'street', 'website_invoice_search'}
        company = company_env.read(fields_company)[0]

        if helpdesk_env:
            if helpdesk_env.priority == '0':
                priority = 'Baja'
            elif helpdesk_env.priority == '1':
                priority = 'Media'
            elif helpdesk_env.priority == '2':
                priority = 'Alta'
            else:
                priority = ''

            data = {
                'name': helpdesk_env.name,
                'partner': helpdesk_env.partner_id.name,
                'user': helpdesk_env.user_id.name,
                'email': helpdesk_env.partner_id.email,
                'phone': helpdesk_env.phone,
                'team': helpdesk_env.team_id.name,
                'team_leader': helpdesk_env.team_leader_id.name,
                'department': helpdesk_env.department_id.name,
                'project': helpdesk_env.project_id.name,
                'priority': priority,
                'request_date': helpdesk_env.request_date,
                'close_date': helpdesk_env.close_date,
                'description': helpdesk_env.description,
                'company': {
                    'email': company['email'],
                    'website': company['website'],
                    'website_invoice_search': company['website_invoice_search'],
                    'company_registry': company['company_registry'],
                    'contact_address': company['partner_id'][1],
                    'vat': company['vat'],
                    'vat_label': "RUC",
                    'name': company['name'],
                    'street': company['street'],
                    'state_id': company['state_id'] and company['state_id'][1] or "",
                    'city': company['city'],
                    'country_id': company['country_id'] and company['country_id'][1] or "",
                    'phone': company['phone'],
                    'logo':  '/web/image?model=res.company&id={}&field=logo'.format(company['id'])
                },
            }
            return data
        else:
            return {}
