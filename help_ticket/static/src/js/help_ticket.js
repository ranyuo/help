odoo.define('help_ticket.Helpdesck_support_ticket', function(require) {
    "use strict";

    var AbstractAction = require('web.AbstractAction');
    var field_utils = require('web.field_utils');
    var utils = require('web.utils');
    var ajax = require('web.ajax');
    var core = require('web.core');
    var rpc = require('web.rpc');
    var session = require('web.session');
    var web_client = require('web.web_client');
    var _t = core._t;
    var QWeb = core.qweb;
    var round_di = utils.round_decimals;

    var HelpTicket = AbstractAction.extend({
        hasControlPanel: true,
        contentTemplate: 'HelpTicketMain',
        events: {
            "click .back":"btn_back",
            "click .print":"btn_print"
        },

        init: function(parent, action, options) {

            this.ticket_id = action.params.ticket_id
            this.model_id = action.params.model_id
            this._super.apply(this, arguments);
            this._title = "Ticket"
        },

        start: function() {
            var self = this;
            this.set("title", 'Dashboard');
            return this._super().then(function() {
                self.render_dashboards();
                self.render_graphs();
                self.$el.parent().addClass('oe_background_grey');
            });
        },
        btn_back:function(){
            if (this.controlPanelParams.breadcrumbs[this.controlPanelParams.breadcrumbs.length-1]){
                this.trigger_up('breadcrumb_clicked', {controllerID: this.controlPanelParams.breadcrumbs[this.controlPanelParams.breadcrumbs.length-1].controllerID});
            }
        },
        btn_print: function(){
            window.print();
        },
        render_dashboards: function() {
            console.log('render_dashboards')
            var self = this;
            var templates = []
            if (self.is_manager == true) { templates = ['HelpScreenWidget']; } else { templates = ['HelpScreenWidget']; }
            _.each(templates, function(template) {
                self.$('.screens').append(QWeb.render(template, { widget: self }));
            });
        },
        render_graphs: function() {
            console.log("RENDER_GRAPHS")
            var self = this;
            self.render_ticket();
        },
        render_ticket: function() {
            var self = this;
            var param = {}
            param = {
                    model: 'helpdesk.support',
                    method: 'helpdesk_data',
                    args: [self.ticket_id]
                }
            rpc.query(param).then(function(result) {
                if (result) {
                    self.$('.pos-receipt-container').append(QWeb.render('ReceiptHelp', {receipt: result, widget: self}));
                }

            });
        },
    });

    core.action_registry.add('help_ticket', HelpTicket);

    return HelpTicket;
    });
